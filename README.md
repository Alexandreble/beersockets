# Getting started with beerSockets

BeerSockets and a simple websockets server written in java spring-boot.  
It broadcasts on a topic every 10 seconds a price, a discount price of beer and its id.  
The selection of returned beers is random in the dataset.

****
## Requirements
You need Docker, please install Docker on your computer:  
* [Install Docker on MacOs](https://docs.docker.com/desktop/mac/install/)
* [Install Docker on Windows](https://docs.docker.com/desktop/windows/install/)

****
## Installation and startup 
Clone the project on your machine:
```shell
git clone https://gitlab.com/Alexandreble/beersockets.git
```

### Build the image
Start your Docker engine.  
Go to the root of the cloned project:
```shell
cd /foo/bar/beersockets
```  
Then build the image:
```shell
docker build -t beersockets:latest .
```  

### Start the image
```shell
docker run -p 8081:8091 beersockets    
```  

**The server is now active.**  
You can monitor the logs and ensure the proper functioning of the server in the console where you started the Docker image

![Logs example.](https://i.ibb.co/crqbZDz/Capture-d-cran-2022-02-08-112743.png)

*****

## How to consume websockets
This server is configured so that websockets are compatible with SockJS.  
You can also use alternatives to this package.

* [SockJS-client reference guide](https://www.npmjs.com/package/sockjs-client)

### Consumption information

**Where is the server ?**  
The server running on *localhost:8081* or *local.localhost:8081*

**Where is the endpoint to connect to?**  
The server endpoint is : 
>/beers  

**What to subscribe?**  
You can subscribe the following topic:
>/topic/beers  

****

## Data Setup
While using the app, 42 different beers prices can be returned to you.  
If you want more fun, you can edit the datasource:  

* [Edit beer dataset](https://www.npoint.io/docs/24df3a7e2c19c608f23d)  
> :warning: Be sure to follow the data schema. If you mess it up, the websocket server may return errors  

**Model of a beer object in the dataset:**
```JSON
{"id":"int","price":"int","discountPrice": "int"}
```  
**Example:**
```JSON
{"id":"42","price":"42.99","discountPrice": "2.00"}
```  