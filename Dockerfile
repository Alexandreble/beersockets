FROM openjdk:17
LABEL maintainer="Alexandre Blerreau <alexandre.blerreau@noconsulting.fr>"
ADD target/beerSockets-0.0.1-SNAPSHOT.jar beerSockets.jar
ENTRYPOINT ["java", "-jar", "beerSockets.jar"]
