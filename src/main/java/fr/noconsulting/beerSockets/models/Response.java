package fr.noconsulting.beerSockets.models;

/**
 * Response model.
 */
public class Response {

    private Beer content;

    /**
     * ResponseModel class constructor.
     *
     * @param content content of the message to send.
     */
    public Response(Beer content) {
        this.content = content;
    }

    /**
     * Gets the content of the message.
     *
     * @return The content value.
     */
    public String getContent() {
        return "{" +
                "\"Id\":" + content.GetId() + "," +
                "\"Price\":" + content.GetPrice() + "," +
                "\"DiscountPrice\":" + content.GetDiscountPrice() +
                '}';
    }


}
