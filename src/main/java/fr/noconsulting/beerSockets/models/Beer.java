package fr.noconsulting.beerSockets.models;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Beer object model.
 */
@Entity
public class Beer {

    @Id
    private int id;
    private double price;
    private double discountPrice;

    /**
     * Beer class constructor.
     *
     * @param id The id of the beer.
     * @param price The price of the beer.
     * @param discountPrice the discount price of the beer.
     */
    public Beer(String id, String price, String discountPrice) {
        this.id = Integer.parseInt(id);
        this.price = Double.parseDouble(price);
        this.discountPrice = Double.parseDouble(discountPrice);
    }

    /**
     * Beer class constructor.
     */
    public Beer() {
    }

    /**
     * Gets the id of the targeted beer.
     *
     * @return The uuid of the beer.
     */
    public int GetId() {

        return this.id;
    }

    /**
     * Gets the price of the targeted beer.
     *
     * @return The price of the beer.
     */
    public double GetPrice() {

        return this.price;
    }

    /**
     * Gets the discount price of the targeted beer.
     *
     * @return The discount price of the beer.
     */
    public double GetDiscountPrice() {
        return this.discountPrice;
    }
}
