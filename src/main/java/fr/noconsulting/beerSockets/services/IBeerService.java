package fr.noconsulting.beerSockets.services;

import fr.noconsulting.beerSockets.models.Beer;

import java.util.List;

/**
 * Represent the BeerService.
 */
public interface IBeerService {

    /**
     * Gets the list of possible beers.
     *
     * @return A list of beers.
     */
    List<Beer> GetBeers();

    /**
     * Pick up a random beer from list.
     *
     * @return A random beer.
     */
    Beer GetRandomBeer();

}
