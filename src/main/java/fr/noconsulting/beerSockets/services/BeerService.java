package fr.noconsulting.beerSockets.services;

import fr.noconsulting.beerSockets.models.Beer;
import fr.noconsulting.beerSockets.repositories.BeerRepository;
import fr.noconsulting.beerSockets.repositories.IBeerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Gets beers, on list or random.
 */
@Service
public class BeerService implements IBeerService {

    private static final Logger logger = LoggerFactory.getLogger(BeerService.class);
    private IBeerRepository beerRepository;
    private Random random;
    private List<Beer> beers;

    /**
     * Beer service class constructor.
     *
     * @throws IOException
     */
    public BeerService() throws IOException {
        this.beerRepository = new BeerRepository();
        this.random = new Random();
        this.beers = new ArrayList<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Beer> GetBeers() {
        logger.info("Retrieving the beer list.");
        return beerRepository.GetBeerFromDistantFile();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Beer GetRandomBeer() {
        logger.info("Random recovery of a beer.");

        if (beers.isEmpty()) {
            logger.info("The list of beers is empty: Retrieval of beers.");
            beers = GetBeers();
        }
        return beers.get(random.nextInt(beers.size()));

    }
}
