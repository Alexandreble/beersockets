package fr.noconsulting.beerSockets.configurations;

import fr.noconsulting.beerSockets.models.Response;
import fr.noconsulting.beerSockets.services.BeerService;
import fr.noconsulting.beerSockets.services.IBeerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.IOException;

@EnableScheduling
@Configuration
/**
 * Class grouping the configuration applicable to the scheduler.
 */
public class SchedulerConfiguration {

    private static Logger logger = LoggerFactory.getLogger(SchedulerConfiguration.class);

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    private IBeerService beerService = new BeerService();

    /**
     * SchedulerConfiguration class constructor.
     * @throws IOException
     */
    public SchedulerConfiguration() throws IOException {
    }

    /**
     * Send a message with delay on the topic.
     */
    @Scheduled(fixedRateString = "${socket.delay}", initialDelay = 10000)
    public void sendMessages() {

        logger.info("Sending new message on topic");
        messagingTemplate.convertAndSend("/topic/beers", new Response(beerService.GetRandomBeer()));
    }
}
