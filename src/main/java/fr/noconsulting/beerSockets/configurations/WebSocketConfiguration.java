package fr.noconsulting.beerSockets.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

@Configuration
@EnableWebSocketMessageBroker
/**
 * Class grouping the configuration applicable to the web socket server.
 */
public class WebSocketConfiguration extends AbstractWebSocketMessageBrokerConfigurer {

    @Value("${socket.endPoint}")
    private String BEER_SOCKET_END_POINT;

    @Value("${socket.topic}")
    private String DESTINATION_PREFIX_NAME;

    @Value("${socket.applicationPrefix}")
    private String APPLICATION_DESTINATION_PREFIX_NAME;


    /**
     * Configuring Endpoints for Stomp.
     *
     * @param stompEndpointRegistry
     */
    @Override
    public void registerStompEndpoints(StompEndpointRegistry stompEndpointRegistry) {
        stompEndpointRegistry.addEndpoint(BEER_SOCKET_END_POINT)
                .withSockJS();
    }

    /**
     * MessageBroker configurations.
     *
     * @param messageBrokerRegistry
     */
    @Override
    public void configureMessageBroker(MessageBrokerRegistry messageBrokerRegistry) {
        messageBrokerRegistry.enableSimpleBroker(DESTINATION_PREFIX_NAME);
        messageBrokerRegistry.setApplicationDestinationPrefixes(APPLICATION_DESTINATION_PREFIX_NAME);

    }
}

