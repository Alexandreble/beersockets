package fr.noconsulting.beerSockets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class BeerSocketsApplication {

    public static void main(String[] args) {
        SpringApplication.run(BeerSocketsApplication.class, args);
    }

}
