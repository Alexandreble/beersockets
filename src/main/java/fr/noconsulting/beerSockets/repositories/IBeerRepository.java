package fr.noconsulting.beerSockets.repositories;

import fr.noconsulting.beerSockets.models.Beer;

import java.util.List;

/**
 * Represent the BeerRepository.
 */
public interface IBeerRepository {

    /**
     * Gets the list of possible beers.
     *
     * @return A list of beers.
     */
    List<Beer> GetBeerFromDistantFile();
}
