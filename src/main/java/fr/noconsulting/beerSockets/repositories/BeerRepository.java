package fr.noconsulting.beerSockets.repositories;

import fr.noconsulting.beerSockets.models.Beer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Beers repository.
 */
@Repository
public class BeerRepository implements IBeerRepository {

    private static final Logger logger = LoggerFactory.getLogger(BeerRepository.class);
    private final List<Beer> beers = new ArrayList<>();
    private final static String JSON_SOURCE_URL = "https://api.npoint.io/24df3a7e2c19c608f23d";

    /**
     * BeerRepository class constructor.
     * Retrieves and initializes a list of beers.
     *
     * @throws IOException
     */
    public BeerRepository() throws IOException {
        ReadAndParseJsonData();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Beer> GetBeerFromDistantFile() {
        return this.beers;
    }

    /**
     * Read a remote json file and order the refilling of a list of beer objects.
     *
     * @throws IOException
     */
    private void ReadAndParseJsonData() throws IOException {
        InputStream inputStream = new URL(JSON_SOURCE_URL).openStream();
        try {
            logger.info("Reading json data file...");

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            String jsonText = readAll(bufferedReader);
            JSONArray jsonArray = new JSONArray(jsonText);

            InitializeBeerList(jsonArray);

        } catch (Exception ex) {
            logger.error("An error occurred while reading the beer object received: ${ex.getMessage()}  ${ex} ");
            throw ex;
        } finally {
            inputStream.close();
        }
    }

    /**
     * Populate a beer object list from a received data array.
     *
     * @param jsonArray Table of values received.
     */
    private void InitializeBeerList(JSONArray jsonArray) {
        logger.info("Populate a list of beer from data received.");

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject currentObject = jsonArray.getJSONObject(i);
            beers.add(new Beer(currentObject.getString("id"), currentObject.getString("price"), currentObject.getString("discountPrice")));
        }
    }

    /**
     * Read and build a string.
     *
     * @param reader a buffered reader.
     * @return A string constructed from a full read.
     * @throws IOException
     */
    private String readAll(Reader reader) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = reader.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
}
